<!doctype html>
<html <?php language_attributes(); ?> <?php twentytwentyone_the_html_classes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta property="og:title" content="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
	<meta property="og:type" content="website">
	<meta property="og:url" content="<?php bloginfo('wpurl'); ?>">
	<meta property="og:image" content="<?php echo($GLOBALS['frieda_fp']["panoramalogourl"]) ?>">
	<meta property="og:description" content="Ob für den Einkauf, Umzug oder Kindertransport – Das freie Lastenrad Zittau hilft flexibel und kostenfrei weiter.">
	<meta property="og:locale" content="de_DE">
	<meta name="description" content="Ob für den Einkauf, Umzug oder Kindertransport – Das freie Lastenrad Zittau hilft flexibel und kostenfrei weiter.">
	<meta name="keywords" content="Lastenrad,Transportrad,Verleih,ADFC">
	<meta name="author" content="ADFC Zittau">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'twentytwentyone' ); ?></a>

 <header>
    <a href="/" class="logo"><img src="<?php echo($GLOBALS['frieda_fp']["logourl"]);?>" alt=""></a>
    <div class="nav">
    <nav class="nav-links">
      <a href="/#sogehts">So geht's</a>
      <a href="/#lastenraeder">Lastenräder</a>
      <a href="/#unterstuetzen">Unterstützen</a>
      <a href="/#ueberuns">Über uns</a>
    </nav>
    <nav class="nav-buttons">
 <?php
if ( is_user_logged_in() ) {
    echo '<a href="/artikel" class="button" id="friedabtn1">Buchung</a>';
    echo '<a href="/meine-buchungen" class="button" id="friedabtn2">Konto</a>';
} else {
    echo '<a href="/artikel" class="button" id="friedabtn1">Buchung</a>';
    echo '<a href="/wp-login.php?redirect_to=/" class="button" id="friedabtn2">Login</a>';
}
?>
    </nav>
    </div>
    <div id="slogan"><?php bloginfo('description'); ?>.</div>
  </header>	

	<div id="content" class="site-content">
		<div id="primary" class="content-area">
<?php
$treat_as_frontpage = (is_front_page() or $GLOBALS['frieda_fp']["localized_front_page"]);
if ($treat_as_frontpage) : ?>
<div class="bg-image">
<img src="<?php echo($GLOBALS['frieda_fp']["panoramalogourl"]);?>" alt="">
</div>
<?php endif; ?>

			<main id="main" class="site-main">
