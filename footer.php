<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- #content -->

 <footer class="pagefooter">
    <div class="footercolumn">
      <a href="https://adfc-zittau.de/index.php/kontakt/4-ansprechpartner/8-ortsgruppe-zittau" target="_blank">Kontakt per Formular</a><br>
      <a href="mailto:info@adfc-zittau.de">Kontakt per E-Mail</a><br>
<a href="https://adfc-zittau.de/index.php/impressum">Impressum</a><br>

    </div>
    <div class="footercolumn">
        <a href="https://www.adfc-zittau.de" target="_blank">ADFC Zittau</a><br>
        <a href="https://www.adfc.de/mitgliedschaft/mitglied-werden" target="_blank">Mitglied im ADFC werden</a><br>
		<a href="https://adfc-zittau.de/index.php/8-meldungen/40-freies-lastenrad-fuer-zittau" target="_blank">Ein freies Lastenrad für Zittau</a><br>
    </div>
    <div class="footercolumn">
      <a href="/Nutzungsbedingungen.pdf" target="_blank">Nutzungsbedingungen</a><br>
      <a href="/Datenschutzerklaerung.pdf" target="_blank">Datenschutzerklärung</a><br>
    </div>
    <div class="footercolumn footercolumnlogo">
      Ein Projekt des<br>
      <a href="http://www.adfc-zittau.de" target="_blank"><img src="<?php bloginfo('template_directory') ?>/adfclogo.png" alt="Logo des ADFC Zittau"></a>
    </div>
  </footer>	
	

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
