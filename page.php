<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */
 
 // If for the queried site a custom localization in the theme unter local_customization exists,
// for example local_customization/dresden.php for lastenradsax.de/dresden, then load this php
$queried_object = get_queried_object();
if (is_a($queried_object, 'WP_Post')) {
    // Remove all characters not between 'a' and 'z', converting uppercase to lowercase
    $sanitized_queried_title = strtolower(preg_replace('/[^a-z]+/i', '', $queried_object->post_title));
    if (strlen($sanitized_queried_title) >= 3)
    {
        $filename = get_theme_file_path( "local_customization/$sanitized_queried_title.php" ); 
        if (file_exists($filename)) {
            include_once($filename);
        }
    }
}

get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	get_template_part( 'template-parts/content/content-page' );

	// If comments are open or there is at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) {
		comments_template();
	}
endwhile; // End of the loop.

get_footer();
